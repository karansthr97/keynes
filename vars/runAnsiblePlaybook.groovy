#!groovy

import groovy.json.JsonOutput

def call(ansible, playbook, extraVars = [], credentialsId = '', extras = '') {
  // Any playbook may implement the check-variables facility.
  ansible.extra_files << 'ansible/playbooks/check-variables.yml'

  // List of files to provision.
  String requirements = playbook.replace('.yml', '-requirements.yml')
  ArrayList files = [playbook, requirements, ansible.config, ansible.inventory] + ansible.extra_files

  // Extra vars from files.
  ansible.extra_vars.each { filename ->
    files << filename
    extras += " --extra-vars @${filename}"
  }

  // Inline extra vars.
  extraVars['workspace'] = env.WORKSPACE
  def json = JsonOutput.toJson(extraVars)
  extras += " --extra-vars '${json}'"

  provisionFiles(files)

  withEnv([
    "PROJECT_NAME=${env.JOB_NAME.substring(0, env.JOB_NAME.indexOf('/'))}",
    "ANSIBLE_CONFIG=${ansible.config}",
  ]) {
    if (fileExists(env.WORKSPACE + '/' + requirements)) {
      sh "ansible-galaxy install -r ${requirements}"
    }
    ansiblePlaybook(
      installation: ansible.installation,
      inventory: ansible.inventory,
      playbook: playbook,
      credentialsId: credentialsId,
      colorized: true,
      extras: "${extras} ${ansible.extras}",
    )
  }
}
