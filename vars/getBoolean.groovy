#!groovy

@NonCPS
def call(value) {
  return (value instanceof Boolean) ? value : value == 'true' ? true: false
}
