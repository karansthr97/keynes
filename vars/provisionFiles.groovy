#!groovy

import org.jenkinsci.plugins.configfiles.ConfigFiles
import jenkins.model.Jenkins

def call(files) {
  files.each { file_rel ->
    // 1. Skip files already present in the workspace. Those ones are provided
    // by a repository cloned to the workspace, or were already provisioned.
    file_abs = env.WORKSPACE + '/' + file_rel
    if (!fileExists(file_abs)) {
      // 2. If a configFile exists, provision it.
      String _fileId = file_rel.replace('/', '__')
      if (ConfigFiles.getByIdOrNull(Jenkins.instance, _fileId)) {
        configFileProvider([configFile(fileId: _fileId, targetLocation: file_rel)]) {}
      }
      // 3. Lastly, try and load it from Keynes.
      else {
        String text = loadResource(file_rel)
        if (text) {
          echo "copy ${file_rel} from Keynes"
          writeFile file: file_abs, text: text
        }
      }
    }
    else {
      echo "file ${file_rel} already exists in the workspace"
    }
  }
}
